
public class ContaCorrente {

	private String numero;
	private String agencia;
	private double saldo;
	
	public ContaCorrente() {
		super();
	}

	public ContaCorrente(String numero, String agencia) {
		super();
		this.numero = numero;
		this.agencia = agencia;
	}

	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	
	
	public double getSaldo() {
		return saldo;
	}
	
	public void depositar(double valor) {
		if(valor > 0)
		this.saldo = this.saldo + valor;
		else {
			System.out.println("Valor negativo ou igual a 0");
		}
	}
	
	public boolean sacar(double valor) {
		boolean sucessoSaque = false;
		if(valor <= this.saldo && valor > 0) {
		this.saldo = this.saldo - valor;
		sucessoSaque = true;
		}
		return sucessoSaque;
	
		
	}
	
	public double verSaldo() {
		return this.getSaldo();
	}
}
