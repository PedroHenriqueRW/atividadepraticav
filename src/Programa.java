import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		ContaCorrente conta = new ContaCorrente();
		
		System.out.println("1 - Criar uma conta");
		System.out.println("2 - Depositar");
		System.out.println("3 - Saque");
		System.out.println("4 - Consultar saldo");
		System.out.println("5 - Sair");
		
		System.out.println("");
		
		System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
		
		int opcao =  s.nextInt();
		
		while(opcao != 5) {
			switch (opcao){   
			case 1:
				System.out.println("Informe o n�mero da conta");
				String numeroConta = s.next();
				
				System.out.println("Informe o n�mero da ag�ncia");
				String numeroAgencia = s.next();
				
				System.out.println("Conta criada com sucesso:");
				
				conta = new ContaCorrente(numeroConta, numeroAgencia);
				
				System.out.println("N�mero da conta: " + numeroConta);
				System.out.println("N�mero da ag�ncia: " + numeroAgencia);
				
				System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
            	opcao = s.nextInt();
				
				break;
			case 2: 	
				if(conta.getNumero() != null && conta.getAgencia() != null) {
					System.out.println("Informe o valor do dep�sito:");
	            	double valorDeposito = s.nextDouble();
	            	conta.depositar(valorDeposito);
	            	if(valorDeposito > 0) {
	            	System.out.println("Deposito realizado com sucesso");
	            	}	            
	            	System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
	            	opcao = s.nextInt();
				}else {
					System.out.println("Favor criar uma conta primeiro");
					System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
	            	opcao = s.nextInt();
				}
				break;	
            case 3:
            	if(conta.getNumero() != null && conta.getAgencia() != null) {
            		System.out.println("Informe o valor que voc� deseja sacar:");
                	double valorSaque = s.nextDouble();
                	boolean saqueSucesso = conta.sacar(valorSaque);
                	if(saqueSucesso) {
                		System.out.println("Saque realizado com sucesso");
                	}
                	else {
                		System.out.println("Valor de saque superior ao valor existente na conta ou valor inv�lido");
                	}
                	System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
                    opcao = s.nextInt();
            	}else {
            		System.out.println("Favor criar uma conta primeiro");
            		System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
	            	opcao = s.nextInt();
            	}
                break;
            case 4:
            	if(conta.getNumero() != null && conta.getAgencia() != null) {
            		System.out.println("O seu saldo � R$:" + conta.verSaldo());
                	System.out.println("Consulta realizada com sucesso");
                	System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
                    opcao = s.nextInt();
            	}else {
            		System.out.println("Favor criar uma conta primeiro");
            		System.out.println("Informe a op��o desejada(1,2,3,4 ou 5):");
	            	opcao = s.nextInt();
            	}
                break;
			}	
		}
	}
}
